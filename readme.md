Main CWL reference:
https://www.commonwl.org/

Install cwltool:
https://github.com/common-workflow-language/cwltool

CWL user guide walkthrough:
https://www.commonwl.org/user_guide/

##   Requirements

docker-compose > 3.7: https://docs.docker.com/compose/install/
docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/