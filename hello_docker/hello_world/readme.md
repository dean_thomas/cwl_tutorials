

# docker run

## build


## run

### use default environment variables

```
$ docker run <container-image-name>
```

Gives the output:
```
Hello nobody.
```

### use custom environment variables

```
$ docker run -e WHO=dean <container-image-name>
```

Gives the output:
```
Hello dean.
```

# docker-compose

## build


## run

### use default environment variables

```
docker-compose up
```

Gives the output:

```
Recreating hello_docker_hello_world_1 ... done
Attaching to hello_docker_hello_world_1
hello_world_1  | Hello nobody.
hello_docker_hello_world_1 exited with code 0
```

### use custom environment variables

```
$ WHO=dean docker-compose up
```

Gives the output:
```
Recreating hello_docker_hello_world_1 ... done
Attaching to hello_docker_hello_world_1
hello_world_1  | Hello dean.
hello_docker_hello_world_1 exited with code 0
```