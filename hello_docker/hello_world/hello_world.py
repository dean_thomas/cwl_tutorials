# !/usr/bin/env python
import sys
import os
import logging.handlers
logging.basicConfig(level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler(stream=sys.stdout))

if __name__ == "__main__":
    #   the CRIM platform will provide location(s) to write our output to.  These are passed into the application
    #   as environment variables - the exact variable names are determined by the deployment options.
    #
    #   here we extract the location to write our output file
    out_file = os.environ.get('WPS_myOutput')
    if out_file is None:
        logging.warning("Output environment variable wasn't set, defaulting to 'output.txt'.")
        out_file = "output.txt"

    #   now, begin to produce the output file
    with open(out_file, "w") as f:
        #   dump some information, that will also be useful for debugging
        f.write(str(sys.version_info) + "\n")
        f.writelines([a + "\n" for a in sys.argv])

        #   write a hello world message.  Our docker container is configured to pass arguments from the platform
        #   as individual arguments to the program.  We could, alternatively, reference these directly using
        #   environment variables, as we do with the output filename
        f.write("Hello world." + "\n")
        f.write("Hello {}.\n".format(sys.argv[1] if len(sys.argv) > 1 is not None else "world"))

        #   print the contents of the environment, for additional debugging help
        f.writelines(["{}: {}\n".format(k, v) for k,v in os.environ.items()])

    logging.debug(sys.version_info)
    logging.debug(sys.argv)
    logging.info("Hello {}.".format(sys.argv[1] if len(sys.argv) > 1 is not None else "world"))
